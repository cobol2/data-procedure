       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA4.
       AUTHOR. NAPHAT.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STUDENT-REC-DATA PIC X(44) VALUE "1205621William  Fitzpatrick  
      -    " 19751021LM051385".
       01  LONG-STR PIC X(200) VALUE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      -    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxxxxxxxxxxxxxxxxxxx
      -    "xxxxxxxxxxxxxxxxxxxxxxxxx".
       01  STUDENT-REC.
           05 STUDENT-ID        PIC   9(7).
           05 STUDENT-NAME.
              10 FORENAME       PIC   X(9).
              10 SURNAME.
                 15 F-SURNAME   PIC   X.
                 15 FILLER      PIC   X(11).
           05 DATE-OF-BIRTH.
              10 YOB            PIC   9(4).
              10 MOB-DOB.
                 15 MOB         PIC   99.
                 15 DOB         PIC   99.
           05 COURSE_ID         PIC   X(5).
           05 GPA               PIC   9V99.
       PROCEDURE DIVISION.
           DISPLAY STUDENT-REC-DATA
           MOVE STUDENT-REC-DATA TO STUDENT-REC.
           DISPLAY STUDENT-REC 
           DISPLAY STUDENT-ID 
      *     DISPLAY STUDENT-NAME 
           DISPLAY FORENAME 
           DISPLAY SURNAME 
      *     DISPLAY DATE-OF-BIRTH 
           DISPLAY F-SURNAME "." FORENAME
           DISPLAY DOB "/" MOB "/" YOB
           DISPLAY MOB-DOB 
           DISPLAY COURSE_ID 
           DISPLAY GPA 
           .
